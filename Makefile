#
#  Makefile
#

dist/bundle.zip: MyBot.py ants.py
	mkdir -p dist
	rm -f dist/bundle.zip
	zip dist/bundle.zip MyBot.py ants.py

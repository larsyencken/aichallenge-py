#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  MyBot.py
#  aichallenge.py
#
#  Created by Lars Yencken on 2011-11-02.
#  Copyright 2011 Lars Yencken. All rights reserved.
#

"""
Bot for Google's AI Challenge.
"""

import os
import sys
import heapq
from functools import partial
import random
from collections import defaultdict, namedtuple
from itertools import imap
import logging
import glob

import numpy as np

from ants import Ants, Loc

DEBUG = False

if DEBUG:
    logging.getLogger().setLevel(logging.DEBUG)
    import Image
    for im in glob.glob('game_logs/*.png'):
        os.remove(im)

Order = namedtuple('Order', 'ant dest reason')

class GameTurn(object):
    "Game state for an individual move."
    def __init__(self, ants, game):
        self.ants = ants
        self.game = game
        self.orders = {}
        self.targets = {}
        self.plans = defaultdict(set)
        self.needs_orders = set(ants.my_ants())
        self.frontier = self.build_frontier(self.game.seen)

        # prevent stepping on own hill
        self.plans[1].update(ants.my_hills())

    def do_move_direction(self, loc, direction):
        "Move in a given direction, handling conflicting orders."
        new_loc = self.ants.destination(loc, direction)
        if (self.ants.unoccupied(new_loc) and new_loc not in self.plans[1]):
            self.ants.issue_order((loc, direction))
            assert new_loc not in self.plans[1]
            self.plans[1].add(new_loc)
            assert loc in self.needs_orders
            self.needs_orders.remove(loc)
            return True
        else:
            return False

    def order(self, ant, dest, reason):
        if self.do_move_location(ant, dest):
            logging.info('ordering %s -> %s to %s' % (ant, dest, reason))
            self.orders[ant] = Order(ant, dest, reason)
            return True
        
        return False

    def do_move_location(self, loc, dest):
        "Slow but exact pathfinding."
        p = self.find_path(loc, dest)
        if p:
            logging.debug('finding a path from %s to %s (OK)' % (loc, dest))
            new_loc = p[0]
            d, = self.ants.direction(loc, new_loc)
            if self.do_move_direction(loc, d):
                for i, l in enumerate(p):
                    self.plans[i + 1].add(l)
                self.targets[dest] = loc
                return True
            else:
                logging.debug('but an ant was in the way')

        logging.debug('finding a path from %s to %s (FAIL)' % (loc, dest))
        return False
    
    def do_move_location_fast(self, loc, dest):
        "Greedy pathfinding."
        directions = self.ants.direction(loc, dest)
        for direction in directions:
            if self.do_move_direction(loc, direction):
                self.targets[dest] = loc
                return True
        return False
    
    def find_path(self, loc, dest, max_depth=50):
        "A* search from the location to the destination."
        dest_d = dest.distance
        passable = self.ants.passable
        seen = self.game.seen
        plans = self.plans

        def f(p):
            return dest_d(p[-1]) + len(p)

        def expand(p):
            l0 = p[-1]
            for l in l0.neighbours():
                if l not in p and l not in plans[len(p)] and passable(l) \
                        and seen[l]:
                    yield p + (l,)

        p0 = loc,
        queue = [(f(p0), p0)]
        push = partial(heapq.heappush, queue)
        pop = partial(heapq.heappop, queue)

        visited = set()
        while queue:
            cost, p = pop()
            if p[-1] == dest:
                return p[1:]

            for p_next in expand(p):
                l_next = p_next[-1]
                if l_next not in visited:
                    visited.add(l_next)
                    push((f(p_next), p_next))

    def find_food(self):
        ant_dist = []
        for food_loc in self.ants.food():
            for ant in list(self.needs_orders):
                dist = ant.distance(food_loc)
                ant_dist.append((dist, ant, food_loc))

        ant_dist.sort()
        for dist, ant, food_loc in ant_dist:
            if food_loc not in self.targets \
                    and ant in self.needs_orders:
                self.order(ant, food_loc, 'gather food')

    def attack_hills(self):
        # attack enemy hills
        ant_dist = []
        for hill_loc in self.game.enemy_hills.difference(
                self.game.destroyed_hills):
            for ant in list(self.needs_orders):
                dist = ant.distance(hill_loc)
                ant_dist.append((dist, ant))
        ant_dist.sort()
        for dist, ant in ant_dist:
            if ant in self.needs_orders:
                self.order(ant, hill_loc, 'attack hill')

    def explore(self):
        "Send ants to the frontier."
        if not self.needs_orders:
            return

        frontier = [Loc(y, x) for y, x in zip(*np.nonzero(self.frontier))]
        for ant in list(self.needs_orders):
            if not self.have_time():
                break
            unseen_dist = []
            for l in frontier:
                heapq.heappush(unseen_dist, (ant.distance(l), l))

            while unseen_dist:
                l = heapq.heappop(unseen_dist)[1]
                if self.order(ant, l, 'explore'):
                    break

    def build_frontier(self, seen):
        "Build a list of places to explore on the verge of our knowledge."
        frontier = seen.copy()
        passable = self.ants.passable

        locs = (Loc(i, j) for i, j in zip(*np.nonzero(seen)))
        for l in locs:
            if all(imap(seen.__getitem__, l.neighbours())) \
                    or not passable(l):
                frontier[l] = 0

        if DEBUG:
            dump_grayscale_image(frontier, 'game_logs/%.03d-frontier.png'
                    % self.game.turn_number)
        return frontier

    def print_frontier(self):
        self.game.print_map(self.frontier)

    def have_time(self):
        return self.ants.time_remaining() > 200

    def random_walk(self):
        "Do a random walk with all remaining ants."
        for loc in list(self.needs_orders):
            directions = ['n', 's', 'e', 'w']
            random.shuffle(directions)
            for d in directions:
                if self.do_move_direction(loc, d):
                    break

class MyBot(object):
    "Game state bot, persistent across turns."
    def __init__(self):
        self.seen = None
        self.turn_number = 0
    
    def do_setup(self, ants):
        "Initialise state with game variables."
        self.rows = ants.rows
        self.cols = ants.cols

        self.seen = np.zeros((self.rows, self.cols), dtype=np.bool_)
        self.destroyed_hills = set()
        self.enemy_hills = set()

    def do_turn(self, ants):
        "Give orders to all of our ants."
        self.turn_number += 1
        self.remember_hills(ants)
        self.remember_seen(ants)

        turn = GameTurn(ants, self)
        turn.find_food()
        turn.attack_hills()
        turn.explore()
        turn.random_walk()

    def remember_seen(self, ants):
        visible = ants.visible
        seen = self.seen
        for loc in filter(visible, ants.iter_locs()):
            seen[loc] = True

        if DEBUG:
            dump_grayscale_image(seen, 'game_logs/%.03d-visible.png'
                    % self.turn_number)

    def remember_hills(self, ants):
        for hill_loc, hill_owner in ants.enemy_hills():
            self.enemy_hills.add(hill_loc)

        self.destroyed_hills.update(self.enemy_hills.intersection(
            ants.my_ants()))

def dump_grayscale_image(m, filename):
    rows, cols = m.shape
    im = Image.new('1', (cols, rows))
    for y, x in zip(*np.nonzero(m)):
        im.putpixel((x, y), 1)
    im.save(filename)

if __name__ == '__main__':
    args = sys.argv[1:]
    # psyco will speed up python a little, but is not needed
    try:
        import psyco
        psyco.full()
    except ImportError:
        pass
    
    try:
        if args:
            Ants.run(MyBot(), open(args[0]))
        else:
            Ants.run(MyBot())
    except KeyboardInterrupt:
        print('ctrl-c, leaving ...')


#!/bin/bash
#
#  fix_map.sh
#
#  Fix the supplied maps so as to work with the Python game runner.
#

if [ $# -ne 1 ]; then
  echo 'Usage: fix_map.sh file.map'
  exit 1
fi

source_file="$1"
dest_file="$(dirname $1)/$(basename $1 .map)_fix.map"
echo "${source_file} --> ${dest_file}"
head -n 3 "${source_file}" >"${dest_file}"
tail -n +4 "${source_file}" | tr [0-9] [A-J] >>"${dest_file}"

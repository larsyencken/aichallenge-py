#!/bin/bash
#
#  play_map.sh
#

if [ $# -ne 1 ]; then
  echo 'Usage: play_map.sh <map_type>'
  exit 1
fi

case $1 in
  large)
    python tools/playgame.py "python MyBot.py" "python tools/sample_bots/python/HunterBot.py" --fill --map_file tools/maps/multi_hill_maze/maze_08p_01_fix.map --log_dir game_logs --turns 200 --scenario --food random --player_seed 7 --verbose -e
    ;;
  small)
    python tools/playgame.py "python MyBot.py" "python tools/sample_bots/python/HunterBot.py" --fill --map_file tools/maps/example/tutorial1.map --log_dir game_logs --turns 60 --scenario --food none --player_seed 7 --verbose -e
    ;;
  *)
    printf 'Unknown map "%s"\n' $1
    ;;
esac
